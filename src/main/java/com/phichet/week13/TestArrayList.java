package com.phichet.week13;

import java.util.ArrayList;
import java.util.Collections;

public class TestArrayList {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList();
        list.add("A1");
        list.add("A2");
        list.add("A3");

        for(int i = 0;i<list.size();i++){
            System.out.println(list.get(i));
        }
        System.out.println();
        list.add(0,"A0");
        list.add("A3");
        for(String Str : list){
            System.out.println(Str);
        }
        System.out.println();

        String[] arr =  list.toArray(new String[list.size()]) ;
        for(int i = 0;i<arr.length;i++){
            System.out.println(arr[i]);
        }
        System.out.println();
        list.remove("A0");
        System.out.println(list);
        list.remove(2);
        System.out.println(list);
        list.set(1, "A123");
        System.out.println(list);

        System.out.println();
        ArrayList<String> list2 = new ArrayList(list);
        list2.add("A4");
        System.out.println(list2);
        list2.addAll(list);
        System.out.println(list2);

        System.out.println(list2.contains("A123"));
        System.out.println(list2.contains("A0"));
        System.out.println(list2.indexOf("A3"));
        System.out.println(list2.indexOf("A10"));

        Collections.sort(list2);
        System.out.println(list2);
        Collections.shuffle(list2);
        System.out.println(list2);
        Collections.reverse(list2);
        System.out.println(list2);
        Collections.swap(list2, 0, 3);
        System.out.println(list2);
    }
}
