package com.phichet.week13;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Collections;

public class TestHashSet {
    public static void main(String[] args) {
        HashSet<String> set = new HashSet<>();
        set.add("A1");
        set.add("A2");
        set.add("A3");
        print(set);

        set.add("A0");
        System.out.println(set);
        System.out.println(set.contains("A2"));
        System.out.println(set);
        set.remove("A2");
        System.out.println(set);
        set.add("A4");
        System.out.println(set);

        HashSet<Integer> set2 = new HashSet<>();
        set2.add(1);
        set2.add(2);
        set2.add(3);
        System.out.println(set2);
        set2.add(4);
        System.out.println(set2);
        set2.clear();
        System.out.println(set2);;

        HashSet<String> set3 = new HashSet<>();
        set3.addAll(set);
        System.out.println(set3);
    }
    private static void print(HashSet<String> set) {
        Iterator<String> iterator = set.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
        System.out.println();
    }
    
}
