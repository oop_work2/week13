package com.phichet.week13;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class TestHashMap {
    public static void main(String[] args) {
        HashMap<String , String> map = new HashMap();
        map.put("A1", "1");
        map.put("B2", "2");
        map.put("C3", "3");
        map.put("D4", "4");

        System.out.println(map);
        Set<String> keys = map.keySet();
        Iterator<String> iterator = keys.iterator();
        while(iterator.hasNext()){
            String key = iterator.next();
            System.out.println(key + "=" + map.get(key));
        }
        System.out.println(map.get("A1"));
        System.out.println(map.isEmpty());
    }
}
